# FARGO-FLS #

#### The dust modelling focused fork of FARGO3D out of QMUL. 


------------------------

##### FARGO3D Website: [fargo.in2p3.fr](http://fargo.in2p3.fr)

##### [Documentation](https://fargo3d.bitbucket.io/)

------------------------
## Description
### F - Fast
### L - Locally isothermal
### S - Single Fluid

This fork of FARGO3D was born to implement the single fluid formalism derived in Lin & Youdin (2017), to accelerate high resolution 2D and 3D dusty gas computations, which still prove challenging using multi-fluid formalisms. Read the publication at [MNRAS](https://doi.org/10.1093/mnras/stz2035) or [arxiv](https://arxiv.org/abs/1907.09541).

------------------------
### Maintainers

Francesco Lovascio : f.lovascio@qmul.ac.uk
Colin McNally : c.mcnally@qmul.ac.uk
Kevin Chan : k.chan@qmul.ac.uk

------------------------
### Checkpointing and Wallclock Timeout

One new feature in this version is support for writing checkpoints on a wall clock interval. Always build this version in `para`. Always build this version with `-DMPIIO`. Wall clock checkpointing is controlled by the parameter:

    CHECKPOINTWALLTIME      15

in seconds. This default is ridiculously short to make sure you set it in your problem specific parameter file. The strategy used is to write to the next output file number due to be written (the ones written at the interval `Ninterm`), and keep overwriting that file until `DT*Ninterm`. Thus, you will find the last output file often written too early. There are also two extra files written, `mainloopindex%d.dat` and `snapplanet%d.dat` which are used to restart at the right main `DT` loop index, and restart the planet system correctly. `mainloopindex%d.dat` also tells you when the output was written in multiples of `DT`. 

The code will exit on the first `DT` where the wall clock runtime exceeds another parameter:

    MAXWALLTIME             60

This is also short by default to make sure you set it.

In a batch queue, you can now set up a chain of jobs which depend on each other to push your run forwards, just set `MAXWALLTIME` to a few minutes less than the job run time, and set ` CHECKPOINTWALLTIME` to something reasonable like half an hour. In your job scripts, you can figure out what the latest output to restart from with a bit of bash scripting in the job file like so:

    export RESTART=$(ls -1 output/mainloopindex*.dat | grep -o "[[:digit:]]*" | sort -n | tail -1)
    echo Will restart from output $RESTART

    mpirun -np $NSLOTS ./fargo3d -S $RESTART fargo.par

------------------------
### OpenMP for Hybrid MPI/OpenMP Parallelism

This fork has `c2omp.py` enabled to work like c2cuda generating OpenMP verisons of kernels, with:

    make OMP=1

Then run using `std/func_arch_omp.cfg` and setting the environment variable `OMP_NUM_THREADS=` as appropriate. On stdout a timing in updates-per-second is printed, which will allow you to tune the MPI rank/OpenMP thread mix for your platform/problem. The right mix is not obvious, and must be found through experimentation. You can also always just manually add OpenMP  pragmas to the code wherever you like, which may be simpler for new routines, but won't work with `c2cuda.py`.
