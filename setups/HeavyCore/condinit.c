#include "fargo3d.h"

void Init() {

  real* y1 = Y1->field_cpu;
  real* y2 = Y2->field_cpu;
  real* y3 = Y3->field_cpu;
  real* y4 = Y4->field_cpu;
  OUTPUT(Density);
  OUTPUT(Energy);
  OUTPUT(Vx);
  OUTPUT(Vy);

  int pitch  = Pitch_cpu;
  int stride = Stride_cpu;
  int size_x = XIP; 
  int size_y = Ny+2*NGHY-1;
  int size_z = Nz+2*NGHZ-1;
  
  real* vx_half = Vx_half->field_cpu;
  
  int i,j,k;
  real r, omega;
  real soundspeed;

  real *vphi = Vx->field_cpu;
  real *vr   = Vy->field_cpu;
  real *rho  = Density->field_cpu;
  double dt = DT;
  real *P   = Energy->field_cpu;
  double interface=INTERFACE;
  i = j = k = 0;

  for (j=0; j<Ny+2*NGHY; j++) {
    for (i=0; i<Nx+2*NGHX; i++) {
      r = Ymed(j);
      double fdOut = 0.1;
      double fdIn = 0.5;
      rho[l] = r>interface?50.0:10.0;
      vphi[l] = OMEGAFRAME*r;
      vphi[l] *= (1.0+NOISE*(drand48()-.5));
      P[l] = rho[l]*r;
      vr[l] = NOISE*(drand48()-.5);
      y1[l]	 =P[l];
      y2[l]	 =P[l];
      y3[l]	 =P[l];
      y4[l]	 =P[l];
      double vphi = .25*(vx_half[l] + vx_half[lxp] + vx_half[lym] + vx_half[lxp-pitch]);
      vphi += ymin(j)*OMEGAFRAME;
      double vy_temp = (dt/rho[l])*(P[l]-P[lym])/(ymed(j)-ymed(j-1));
      vy_temp += vphi*vphi/ymin(j)*dt;
      vy_temp /= (1.+VERTICALDAMPING*dt);
      printf("the value here is %f",vy_temp);
    }
  } 
}

void CondInit() {
  Fluids[0] = CreateFluid("gas",GAS);
  SelectFluid(0);
  Init();
}
