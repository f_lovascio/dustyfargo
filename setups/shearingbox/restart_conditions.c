/*
 * =====================================================================================
 *
 *       Filename:  restart_conditions.c
 *
 *    Description:  
 *
 *       Created:  01/31/2020 14:53:00
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Francesco, 
 *   Organization:  QMUL
 *
 * =====================================================================================
 */
#include "fargo3d.h"
#define FLOAT_PI 3.14159265358979 

//Restart condition for rescaling soundspeed without changing dust to gas ratios. 
//Computes old dust fraction, then uses it to compute new pressure using prescribed cs.

void RestartConditions() {
	//printf("Running restart conditions script:\n");
	real* rho = Density->field_cpu;
  real* P = Energy->field_cpu;
  double CSold = CSOLD;
  double tempFD = 0.0;
	double new_CS = CSCONST;
  unsigned int k,j,i;
	for(k=0; k<Nz + 2*NGHZ; k++) {
    for(j=0;j<Ny + 2*NGHY; j++) {
      for(i=0;i<Nx; i++) {
        unsigned int	ll = l;
        tempFD=1.0-(P[ll]/(CSold*CSold*rho[ll]));
				P[ll]=(1.0-tempFD)*(new_CS*new_CS*rho[ll]);
      }
    }
  }
	//printf("Done!\n");
}

