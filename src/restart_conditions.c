#include "fargo3d.h"

void RestartConditions() {
  mastererr ("\n\n******\n\nERROR\n\n");
  mastererr ("You have turned on the RESTARTCONDIN+TIONS flag but \nnot specified restart conditions.\n");
  mastererr ("These must be defined in the setup directory, in a\n");
  mastererr ("file called restart_conditions.c, with a function void RestartConditions().\n");
  mastererr ("\nOnce this is done, issue a 'make clean' before rebuilding.\n");
  prs_exit (1);
}

